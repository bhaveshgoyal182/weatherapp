const input = document.querySelector("#location");
const getBtn = document.querySelector("#getBtn");
// 4ba908bd0f4064bd8d8c2153a6ec2b99
getBtn.addEventListener("click", setQuery);
function setQuery() {
  let place = input.value;
  if(place == "")
    alert("Enter a valid city!");
  else{
  input.value = "";
  let url = `https://api.openweathermap.org/data/2.5/weather?q=${place}&units=metric&appid=4ba908bd0f4064bd8d8c2153a6ec2b99`;
  fetch(url)
    .then((response) => {
      return response.json();
    })
    .then(displayResults);}
}

function displayResults(weather) {
  console.log(weather);
  let date = document.querySelector("#date");
  date.innerText = dateBuilder(new Date());

  let city = document.querySelector("#city");
  city.innerText = `${weather.name},${weather.sys.country}`;

  let temp = document.querySelector("#temp");
  temp.innerText = `${weather.main.temp}°C`;

  let hiLow = document.querySelector("#hi-low");
  hiLow.innerText = `${Math.round(weather.main.temp_min)}°C(min) / ${Math.round(weather.main.temp_max)+1}°C(max)`;

  let windSpeed = document.querySelector("#wind-speed");
  windSpeed.innerText = `${Math.round(weather.wind.speed)} KMs`;

  let weatherType = document.querySelector("#weather-type");
  weatherType.innerText = `${weather.weather[0].main}`;

  if (weatherType.textContent == "Clear" || weatherType.textContent == "Mist") {
    document.body.style.backgroundImage =
      "url('./images/clearSky.jpeg')";
  } else if (weatherType.textContent == "Clouds") {
    document.body.style.backgroundImage =
      "url('./images/clouds.jpeg')";
  } else if (weatherType.textContent == "Haze") {
    document.body.style.backgroundImage =
      "url('./images/Haze.jpeg')";
  } else if (weatherType.textContent == "Rain") {
    document.body.style.backgroundImage =
      "url('./images/Rain.jpeg')";
  } else if (weatherType.textContent == "Snow") {
    document.body.style.backgroundImage =
      "url('./images/Snow.jpeg')";
  } else if (weatherType.textContent == "Thunderstorm") {
    document.body.style.backgroundImage =
      "url('./images/ThunderStorm.jpeg')";
      
  }
}

function dateBuilder(d) {
  let months = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ];
  let days = [
    "Sunday",
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
  ];

  let day = days[d.getDay()];
  let date = d.getDate();
  let month = months[d.getMonth()];
  let year = d.getFullYear();
  return `${day} ${date} ${month} ${year}`;
}
